This repo is based on [Tensor Flow's tutorial](https://www.youtube.com/watch?v=b2fgMCeSNpY&list=PLJbE2Yu2zumDqr_-hqpAN0nIr6m14TAsd) on Flutter in YouTube. 

To make sure you grasp a language, you must key in the lines by yourself, not just by reading the ready lines.

TR@SOE

2019.2.12