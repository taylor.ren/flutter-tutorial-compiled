import 'package:flutter/material.dart';
import '../models/book_model.dart';
import '../blocs/book_bloc.dart';

class BookList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bloc.getAllBooks();
    return Scaffold(
      appBar:AppBar(
        title: Text('随机推荐的藏书'),
      )
    );
  }
}