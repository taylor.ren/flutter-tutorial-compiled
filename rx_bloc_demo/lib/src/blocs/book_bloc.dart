import '../resources/repository.dart';
import 'package:rxdart/rxdart.dart';
import '../models/book_model.dart';

class BookBloc {
  final _repository=Repository();
  final _bookGetter=PublishSubject<BookModel>();

  Observable<BookModel> get allBooks=>_bookGetter.stream;

  getAllBooks() async {
    BookModel bookModel=await _repository.getRandomBooks();
    _bookGetter.sink.add(bookModel);
  }

  dispose() {
    _bookGetter.close();
  }
}

final bloc=BookBloc();