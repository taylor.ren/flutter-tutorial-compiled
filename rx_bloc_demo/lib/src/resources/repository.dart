import 'dart:async';
import 'book_api_provider.dart';
import '../models/book_model.dart';

class Repository {
  final BookApiProvider bookApiProvider=BookApiProvider();
  Future<BookModel> getRandomBooks() => bookApiProvider.getBookList();
}