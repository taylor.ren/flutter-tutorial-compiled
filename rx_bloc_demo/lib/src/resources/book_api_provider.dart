import 'dart:async';
import 'package:http/http.dart' show Client;
import 'dart:convert';
import '../models/book_model.dart';

class BookApiProvider {
  Client client=Client();

  Future<BookModel> getBookList() async {
    print("Fetching books");
    final response=await client.get("http://api.rsywx.com/book/random/10");
    print(response.body.toString());
    return BookModel.fromJson(json.decode(response.body));
  }
}
