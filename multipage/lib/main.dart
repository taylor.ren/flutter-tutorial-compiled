import 'package:flutter/material.dart';

void main() => runApp(
  MaterialApp(
    home: MyApp(),
    theme: themeData,
  )
);

final ThemeData themeData=ThemeData(
  canvasColor: Colors.lightGreenAccent,
  accentColor: Colors.redAccent,
);

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("主页"),
      ),
      body: Center(
        child: FlatButton(
          child: Text("去第二页"),
          onPressed: () {
            Navigator.push(context, PageTwo());
          },

        ),
      )
    );
  }
}

class PageTwo extends MaterialPageRoute<Null> {
  PageTwo(): super(builder: (BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).canvasColor,
        elevation: 1.0,
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Navigator.push(context, PageThree());
          },
          child: Text("Goto Page 3"),
        ),
      )

    );
  });
}

class PageThree extends MaterialPageRoute<Null> {
  PageThree() : super(builder: (BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Page 3 and last page!"),
        backgroundColor: Theme.of(context).accentColor,
        elevation:2.0,
        actions: <Widget> [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.close)
          )
        ]
      ),
      body: Center(
        child: MaterialButton(
          onPressed: () {
            Navigator.popUntil(context, ModalRoute.withName(Navigator.defaultRouteName));
          },
          child: Text("Go Home!")
        )
      ),
    );
  });
}