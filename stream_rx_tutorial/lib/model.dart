import 'dart:async';

class Model {
  int _counter=0;

  StreamController _streamController=StreamController<int>();

  Stream<int> get counter=>_streamController.stream;

  void increaseCounter() {
    _counter++;
    _streamController.add(_counter);
  }
}