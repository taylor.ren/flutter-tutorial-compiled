import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

void main() => runApp(
  MaterialApp(
    home: RsywxData(),
  )
);

class RsywxData extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RsywxDataState();
  }
}

class RsywxDataState extends State<RsywxData> {
  final String url="http://api.rsywx.com/book/search/title/all/1";
  List data;

  Future<String> getRsywxData() async {
    var res= await http.get(Uri.parse(url), headers: {"Accept": 'application/json'});
    setState((){
      var resBody=json.decode(res.body);
      data=resBody['res']['books'];
    });

    return "Success!";
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("任氏有无轩"),
        backgroundColor: Colors.deepPurpleAccent,
      ),
      body: ListView.builder(
        itemCount: data==null?0:data.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            child: Center(
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(15.0),
                        child: Row(
                          children: <Widget>[
                            Text("ID: "),
                            Text(data[index]['bookid'],
                              style: TextStyle(
                                fontSize: 18.0, 
                                color: Colors.black87
                              )
                            )
                          ],
                        )
                      )
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(15.0),
                        child: Row(
                          children: <Widget>[
                            Text("书名："),
                            Text("${data[index]['title']}",
                              style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.red,
                              )
                            )
                          ],
                        )
                      )
                    )
                  ],
                )
              )
            )
          );
        },
      )
    );
  }

  @override
  void initState() {
    super.initState();
    this.getRsywxData();
  }

}
