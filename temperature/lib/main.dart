import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: TempApp(),
    );
  }
}

class TempApp extends StatefulWidget {
  @override
  _TempAppState createState() => _TempAppState();
}

class _TempAppState extends State<TempApp> {
  double input, output;
  bool fOrC;

  @override
  void initState() {
    super.initState();
    input=0.0;
    output=0.0;
    fOrC=true;
  }

  @override
  Widget build(BuildContext context) {
    TextField inputField=TextField(
      keyboardType: TextInputType.number,
      onChanged: (str) {
        try {
          input=double.parse(str);
        } catch (e) {
          input=0.0;
        }
      },
      decoration: InputDecoration(
        labelText: "Input a value in ${fOrC==false?"Fahreheit":"Celsius"}",
      ),
      textAlign: TextAlign.center,
    );

    AppBar appBar=AppBar(
      title: Text("温度转换"),
    );

    Container tempSwitch=Container(
      padding: EdgeInsets.all(15.0),
      child: Row(
        children: <Widget>[
          Text("F"),
          Radio<bool>(
            groupValue: fOrC,
            value: false,
            onChanged: (v) {
              setState(() {
                fOrC=v;
              });
            },
          ),
          Text("C"),
          Radio<bool>(
            groupValue: fOrC,
            value: true,
            onChanged: (v) {
              setState(() {
                fOrC=v;
              });
            },
          )
        ],
      )
    );

    Container calcBtn=Container(
      child: RaisedButton(
        color: Colors.lime,
        child: Text("计算"),
        onPressed: () {
          setState(() {
            fOrC==false 
              ?output=(input -32)*5/9
              :output=(input*9)/5+32;
          });
          AlertDialog dialog=AlertDialog(
            content: fOrC==false
              ? Text("${input.toStringAsFixed(2)} F : ${output.toStringAsFixed(2)} C")
              : Text("${input.toStringAsFixed(2)} C : ${output.toStringAsFixed(2)} F")
          );
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return dialog;
            }
          );
          // Please note the original "child" property is deprecated and should use "builder" instead. 
        },
      )
    );

    return Scaffold(
      appBar: appBar,
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            inputField,
            tempSwitch,
            calcBtn,
          ],
        )
      )
    );
  }
}

